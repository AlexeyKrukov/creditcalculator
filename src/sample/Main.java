package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Main extends Application {

    private Stage window;

    //Элементы окна
    Label sumOfCredit;
    Label rate; //Ставка кредита
    Label period; //Срок кредита
    Label advancedSetting; //Доп. параметры
    Label commision; //Единовременная комиссия
    Label startOfPayment; //Начало выплат

    TextField sumOfCreditField;
    TextField rateField;
    TextField periodField;
    TextField commisionField;

    ComboBox sumOfCreditBox;
    ComboBox periodBox;
    ComboBox commisionBox;
    ComboBox startOfPaymentMonthBox;
    ComboBox startOfPaymentYearBox;

    Button calculateButton;

    //Новые элементы (после вычислений)
    Label regularPayment;


    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Кредитный калькулятор");

        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(10);
        gridPane.setHgap(12);

        sumOfCredit = new Label("Сумма кредита:");
        GridPane.setConstraints(sumOfCredit, 0, 0);

        sumOfCreditField = new TextField();
        GridPane.setConstraints(sumOfCreditField, 1, 0);

        sumOfCreditBox = new ComboBox<Character>();
        sumOfCreditBox.getItems().addAll('₽', '$');
        sumOfCreditBox.setPromptText("Валюта");
        GridPane.setConstraints(sumOfCreditBox, 2, 0);

        rate = new Label("Ставка, %:");
        GridPane.setConstraints(rate, 0, 1);

        rateField = new TextField();
        GridPane.setConstraints(rateField, 1, 1);

        period = new Label("Срок кредита:");
        GridPane.setConstraints(period, 0, 2);

        periodField = new TextField();
        GridPane.setConstraints(periodField, 1, 2);

        periodBox = new ComboBox<String>();
        periodBox.getItems().addAll("месяц", "год");
        periodBox.setPromptText("Время");
        GridPane.setConstraints(periodBox, 2, 2);

        advancedSetting = new Label("Дополнительные параметры");
        GridPane.setConstraints(advancedSetting, 1, 3);

        commision = new Label("Ед.вр. комиссия:");
        GridPane.setConstraints(commision, 0, 4);

        commisionField = new TextField();
        GridPane.setConstraints(commisionField, 1, 4);

        commisionBox = new ComboBox<Character>();
        commisionBox.getItems().addAll('₽', '$');
        commisionBox.setPromptText("Валюта");
        GridPane.setConstraints(commisionBox, 2, 4);

        startOfPayment = new Label("Начало выплат:");
        GridPane.setConstraints(startOfPayment, 0, 5);

        startOfPaymentMonthBox = new ComboBox<String>();
        startOfPaymentMonthBox.getItems().addAll("Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь");
        startOfPaymentMonthBox.setPromptText("Месяц");
        GridPane.setConstraints(startOfPaymentMonthBox, 1, 5);

        calculateButton = new Button("Рассчитать");
        calculateButton.setOnAction(event -> {
            regularPayment = new Label("Регулярный платёж " + "(" + periodBox.getValue() + "):");
            GridPane.setConstraints(regularPayment, 0, 6);
        });
        //Растягиваем кнопку
        calculateButton.setMaxWidth(Double.MAX_VALUE);
        GridPane.setConstraints(calculateButton, 1, 6);

        gridPane.getChildren().addAll(sumOfCredit, sumOfCreditField, sumOfCreditBox, rate, rateField, period, periodField, periodBox, advancedSetting, commision, commisionField, commisionBox, startOfPayment, startOfPaymentMonthBox, calculateButton);
        window.setScene(new Scene(gridPane, 400, 400));
        window.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
