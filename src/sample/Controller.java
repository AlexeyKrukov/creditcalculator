package sample;

public class Controller {

    /**
     * @param credit Количество денег, взятое в кредит
     * @param procent Процент по кредиту
     * @param time Время выплаты кредита
     * @return Платёж в typeOfTime
     */
    double getRegularPayment(double credit, int procent, int time) {
        double overpayment = getOverpayment(credit, procent);
        double creditWithProcent = credit + overpayment;
        double regularPayment = creditWithProcent / time;
        return regularPayment;
    }

    /**
     * @param credit Количество денег, взятое в кредит
     * @param procent Процент по кредиту
     * @return Переплату по кредиту
     */
    double getOverpayment(double credit, int procent) {
        return credit * (100 + procent) / 100;
    }
}
